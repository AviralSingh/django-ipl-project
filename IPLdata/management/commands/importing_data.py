import csv
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from IPLdata.models import Match, Delivery

class Command(BaseCommand):
    help = 'Import datasets into tables using atomic transactions'

    def handle(self, *args, **options):
        try:
            with transaction.atomic():
                # Import matches dataset first
                with open('/home/aviral/Desktop/Django IPL Project/trydjango/src/datasets/matches.csv', 'r') as file:
                    reader = csv.reader(file)
                    next(reader)  

                    for row in reader:
                        id, season, city, date, team1, team2, toss_winner, toss_decision, \
                        result, dl_applied, winner, win_by_runs, win_by_wickets, \
                        player_of_match, venue, umpire1, umpire2, umpire3 = row

                        match = Match.objects.create(
                            id=id, season=season, city=city, date=date, team1=team1,
                            team2=team2, toss_winner=toss_winner, toss_decision=toss_decision,
                            result=result, dl_applied=dl_applied, winner=winner,
                            win_by_runs=win_by_runs, win_by_wickets=win_by_wickets,
                            player_of_match=player_of_match, venue=venue, umpire1=umpire1,
                            umpire2=umpire2, umpire3=umpire3
                        )

                # Import deliveries dataset
                with open('/home/aviral/Desktop/Django IPL Project/trydjango/src/datasets/deliveries.csv', 'r') as file:
                    reader = csv.reader(file)
                    next(reader)  

                    for row in reader:
                        match_id, inning, batting_team, bowling_team, over, ball, \
                        batsman, non_striker, bowler, is_super_over, wide_runs, bye_runs, \
                        legbye_runs, noball_runs, penalty_runs, batsman_runs, extra_runs, \
                        total_runs, player_dismissed, dismissal_kind, fielder = row

                        match = Match.objects.get(id=match_id)

                        Delivery.objects.create(
                            match_id=match, inning=inning, batting_team=batting_team,
                            bowling_team=bowling_team, over=over, ball=ball, batsman=batsman,
                            non_striker=non_striker, bowler=bowler, is_super_over=is_super_over,
                            wide_runs=wide_runs, bye_runs=bye_runs, legbye_runs=legbye_runs,
                            noball_runs=noball_runs, penalty_runs=penalty_runs,
                            batsman_runs=batsman_runs, extra_runs=extra_runs, total_runs=total_runs,
                            player_dismissed=player_dismissed, dismissal_kind=dismissal_kind,
                            fielder=fielder
                        )

            self.stdout.write(self.style.SUCCESS('Successfully imported datasets'))

        except Exception as e:
            raise CommandError(f'Error importing datasets: {e}')
