from django.urls import path
from .views import matches_played_per_year_view,matches_won_per_team_per_year_view,extra_runs_conceded_per_team,top_economical_bowlers_2015,matches_played_per_year_chart,matches_won_per_team_per_year_chart,extra_runs_conceded_per_team_chart,top_economical_bowlers_2015_chart

urlpatterns = [
    path('matches/', matches_played_per_year_view, name='matches_per_year'),
    path('matchesWon/', matches_won_per_team_per_year_view, name='matches_won_per_team_per_year'),
    path('extraRuns/', extra_runs_conceded_per_team, name='extra_runs_conceded_per_team'),
    path('topbowlers/', top_economical_bowlers_2015, name='top_economical_bowlers_2015'),
    path('matcheschart/', matches_played_per_year_chart, name='matches_played_per_year_chart'),
    path('matcheswonchart/', matches_won_per_team_per_year_chart, name='matches_won_per_team_per_year_chart'),
    path('extrarunschart/', extra_runs_conceded_per_team_chart, name='extra_runs_conceded_per_team_chart'),
    path('topbowlerschart/', top_economical_bowlers_2015_chart, name='top_economical_bowlers_2015_chart'),
]
