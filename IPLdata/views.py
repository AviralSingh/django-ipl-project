import json
from django.shortcuts import render
from .models import Match, Delivery
from django.http import JsonResponse
from django.db.models import Count, Sum, F, ExpressionWrapper, FloatField
# Create your views here.


def matches_played_per_year_view(request):
    matches_per_year = Match.objects.values(
        'season').annotate(matches_count=Count('id'))
    result = [{'year': match['season'], 'matches_played': match['matches_count']}
              for match in matches_per_year]

    return JsonResponse(result, safe=False)

def matches_won_per_team_per_year_view(request):
    matches_won = Match.objects.values('season', 'winner').annotate(
        matches_won_count=Count('id')).order_by('season', 'winner')

    result = {}
    
    for match in matches_won:
        year = str(match['season'])
        team = match['winner']
        matches_won_count = match['matches_won_count']

        if year not in result:
            result[year] = {}

        result[year][team] = matches_won_count

    return JsonResponse(result, safe=False)


def extra_runs_conceded_per_team(request):
    extra_runs_conceded = Delivery.objects.filter(match_id__season=2016).values(
        'batting_team').annotate(total_extra_runs=Sum('extra_runs'))

    result = [
        {
            'team': run['batting_team'],
            'extra_runs_conceded': run['total_extra_runs']
        }
        for run in extra_runs_conceded
    ]

    return JsonResponse(result, safe=False)


def top_economical_bowlers_2015(request):
    economy_query = Delivery.objects.filter(match_id__season=2015).values('bowler').annotate(
        total_runs_conceded=Sum('total_runs'),
        total_balls=Count('ball'),
        total_bye_runs=Sum('bye_runs'),
        total_legbye_runs=Sum('legbye_runs'),
        economy_rate=ExpressionWrapper(
            (F('total_runs_conceded') - F('total_bye_runs') - F('total_legbye_runs')) /
            (F('total_balls') / 6.0),
            output_field=FloatField()
        )
    ).order_by('economy_rate')[:10]

    result = [
        {
            'bowler': record['bowler'],
            'economy_rate': round(record['economy_rate'], 2)
        }
        for record in economy_query
    ]

    return JsonResponse(result, safe=False)

# charts views


def matches_played_per_year_chart(request):
    response = matches_played_per_year_view('matches/')
    chart_data = json.loads(response.content)
    return render(request, 'IPLdata/matches.html', {'chart_data': json.dumps(chart_data)})

def matches_won_per_team_per_year_chart(request):
    response = matches_won_per_team_per_year_view('macthesWon/')
    chart_data = json.loads(response.content)
    return render(request, 'IPLdata/matches_won.html', {'chart_data': chart_data})

def extra_runs_conceded_per_team_chart(request):
    response = extra_runs_conceded_per_team('extraruns/')
    chart_data = json.loads(response.content)
    return render(request, 'IPLdata/extra_runs_chart.html', {'chart_data': chart_data})

def top_economical_bowlers_2015_chart(request):
    response = top_economical_bowlers_2015('topbowlers/')
    chart_data = json.loads(response.content)
    return render(request, 'IPLdata/top_economical_bowlers_chart.html', {'chart_data': chart_data})



